# Jobs API app


## Installation

Follow these easy steps to install and start the app:
### Initalize admin email and password
    export admin_email="email@email.com"
    export admin_password="password"
### Set up Rails app

First, install the gems required by the application:

    bundle install

Next, execute the database migrations/schema setup:

	bundle exec rake db:setup


### Start the app

Start the Rails app:

    bundle exec rails server

You can find your app now by pointing to [http://localhost:3000](http://localhost:3000).
You can see the docs of the apis at  http://localhost:3000/api-docs
