# frozen_string_literal: true

class JobApplicationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_job_application, only: %i[show update destroy]

  # GET /applications
  def index
    @job_applications = policy_scope(JobApplication)
    authorize @job_applications
    @job_applications = @job_applications.where(post_id: params[:post_id]) if params[:post_id]
    render json: @job_applications
  end

  # GET /applications/1
  def show
    @job_application.update(status: 1) if current_user.admin?
    render json: @job_application
  end

  # POST /applications
  def create
    @job_application = JobApplication.new(post_id: job_application_params['post_id'], user_id: current_user.id)
    authorize @job_application
    if @job_application.save
      render json: @job_application, status: :created, location: @job_application
    else
      render json: @job_application.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /applications/1
  def update
    if @job_application.update(job_application_params)
      render json: @job_application
    else
      render json: @job_application.errors, status: :unprocessable_entity
    end
  end

  # DELETE /applications/1
  def destroy
    @job_application.destroy!
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_job_application
    @job_application = JobApplication.find(params[:id])
    authorize @job_application
  end

  # Only allow a list of trusted parameters through.
  def job_application_params
    params.require(:application).permit(:post_id)
  end
end
