# frozen_string_literal: true

class ApplicationController < ActionController::API
  before_action :skip_cookies
  respond_to :json
  include Pundit::Authorization

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
end

def skip_cookies
  request.session_options[:skip] = true
end

def record_not_found
  render json: { error: 'Record not found' }, status: :not_found
end

def user_not_authorized
  render json: errors_response, status: :forbidden
end

def errors_response
  {
    errors:
    [
      { message: 'You are not authorized to perform this action.' }
    ]
  }
end
