# frozen_string_literal: true

module Users
  class RegistrationsController < Devise::RegistrationsController
    before_action :skip_cookies
    respond_to :json

    def respond_with(resource, _opts = {})
      if resource.persisted?
        render json: {
          status: { code: 200, message: 'Signed up sucessfully.' },
          data: UserSerializer.new(resource).serializable_hash[:data][:attributes]
        }
      else
        render json: {
          status: { message: "User couldn't be created successfully. #{resource.errors.full_messages.to_sentence}" }
        }, status: :unprocessable_entity
      end
    end
  end
end

def skip_cookies
  request.session_options[:skip] = true
end
