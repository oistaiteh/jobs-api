# frozen_string_literal: true

class JobApplicationPolicy < ApplicationPolicy
  def index?
    user.present?
  end

  def show?
    user.present? && (user == record.user || user.admin?)
  end

  def create?
    user.user?
  end

  def update?
    user == record.user && user.user?
  end

  def destroy?
    user == record.user && user.user?
  end

  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      else
        scope.where(user:)
      end
    end
  end
end
