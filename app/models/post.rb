# frozen_string_literal: true

class Post < ApplicationRecord
  has_many :job_applications
  validates :title, presence: true, length: { minimum: 3, maximum: 50 }
  validates :description, presence: true, length: { minimum: 10 }
end
