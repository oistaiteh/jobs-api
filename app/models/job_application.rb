# frozen_string_literal: true

class JobApplication < ApplicationRecord
  belongs_to :post
  belongs_to :user
  validates :post_id, presence: true
  validates :user_id, presence: true
  enum status: { not_seen: 0, seen: 1 }

  validates :user_id, uniqueness: { scope: :post_id, message: 'Job Application already exists for this post.' }

  validate :post_not_archived

  def post_not_archived
    errors.add(:post_id, message: 'Post is archived') if post&.archived
  end
end
