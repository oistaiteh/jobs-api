# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Post, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:description) }
    it { should validate_length_of(:title).is_at_least(3).is_at_most(50) }
    it { should validate_length_of(:description).is_at_least(10) }
  end

  describe 'associations' do
    it { should have_many(:job_applications) }
  end
end
