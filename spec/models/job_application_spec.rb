# frozen_string_literal: true

require 'rails_helper'

RSpec.describe JobApplication, type: :model do
  let(:first_user) do
    User.create(email: 'test2@test.com', password: '123456')
  end
  let(:second_user) do
    User.create(email: 'test3@test.com', password: '123456')
  end
  let(:first_post) do
    Post.create(title: 'First Post', description: 'First Post describtion')
  end
  let(:second_post) do
    Post.create(title: 'Second Post', description: 'Second Post describtion')
  end
  let(:archived_post) do
    Post.create(title: 'Archived Post', description: 'Archived Post describtion', archived: true)
  end
  describe 'validations' do
    it { should validate_presence_of(:post_id) }
    it { should validate_presence_of(:user_id) }
    it 'should validate uniqueness of user_id and post_id' do
      user = first_user
      post = first_post

      JobApplication.create(post_id: post.id, user_id: user.id)
      app = JobApplication.create(post_id: post.id, user_id: user.id)
      expect(app).to_not be_valid
    end
    it 'should allow add multiple job applications for the same post for other users' do
      user = first_user
      post = first_post

      JobApplication.create(post_id: post.id, user_id: user.id)
      app = JobApplication.create(post_id: post.id, user_id: second_user.id)
      expect(app).to be_valid
    end

    it 'should not allow add job application for archived post' do
      user = first_user
      post = archived_post

      app = JobApplication.create(post_id: post.id, user_id: user.id)
      expect(app).to_not be_valid
    end
  end

  describe 'associations' do
    it { should belong_to(:post) }
    it { should belong_to(:user) }
  end

  describe 'enums' do
    it { should define_enum_for(:status).with_values(not_seen: 0, seen: 1) }
  end
end
