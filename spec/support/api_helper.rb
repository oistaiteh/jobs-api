# frozen_string_literal: true

module ApiHelper
  def authenticated_header(header, user)
    Devise::JWT::TestHelpers.auth_headers(header, user)
  end
end
