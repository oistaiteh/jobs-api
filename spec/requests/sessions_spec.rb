# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'sessions', type: :request do
  path '/login' do
    post 'Creates a new session' do
      tags 'Sessions'
      consumes 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: { user: { type: :object, properties: {
          email: { type: :string, example: 'test@email.com' },
          password: { type: :string, example: '123456' }
        },
                              required: %w[email password] } }
      }

      response '200', 'session created' do
        new_user = User.create(email: 'test@email.com', password: '123456')
        let(:user) { { user: { email: new_user.email, password: new_user.password } } }
        run_test!
      end

      response '401', 'invalid request' do
        let(:user) { { user: { email: 'test_2@email.com', password: '123456' } } }
        run_test!
      end
    end
  end

  path '/logout' do
    delete 'Logs out the current user' do
      tags 'Sessions'
      consumes 'application/json'
      security [Bearer: {}]

      response '401', 'session deleted' do
        let(:Authorization) { 'wrong token' }
        run_test!
      end
    end
  end
end
