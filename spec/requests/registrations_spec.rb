# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'registrations', type: :request do
  path '/signup' do
    post 'Creates a new user' do
      tags 'Registrations'
      consumes 'application/json'
      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: { user: { type: :object, properties: {
          email: { type: :string, example: 'test@gmail.com' },
          password: { type: :string, example: '123456' }
        },
                              required: %w[email password] } }
      }

      response '200', 'user created' do
        let(:user) { { user: { email: 'email@email.com', password: 'password' } } }
        run_test!
      end

      response '422', 'invalid request' do
        let(:user) { { user: { 'password' => 'password' } } }
        run_test!
      end
    end
  end
end
