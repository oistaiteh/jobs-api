# frozen_string_literal: true

Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  resources :job_applications, path: 'applications', only: %i[index create destroy show]

  resources :posts

  devise_for :users, path: '', skip: %i[sessions registrations]

  as :user do
    post '/login' => 'users/sessions#create'
    delete '/logout' => 'users/sessions#destroy'
    post '/signup' => 'users/registrations#create'
  end
  #  Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
