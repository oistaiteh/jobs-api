# frozen_string_literal: true

class FixColumnName < ActiveRecord::Migration[7.1]
  def change
    rename_column :job_applications, :seen, :status
  end
end
