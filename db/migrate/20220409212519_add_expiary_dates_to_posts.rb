class AddExpiaryDatesToPosts < ActiveRecord::Migration[7.1]
  def change
    add_column :posts, :expiry_date, :datetime, default: 1.year.from_now
  end
end
